# My recipe library

![](https://media.giphy.com/media/13vSD7ajIJwgb6/giphy.gif)

I'm a telegram bot that helps you organize your favourite food recipes

1. I save your favourite recipes from any site

2. Ask me when you are in need of inspiration. I can give you a random recipe or a recipe containing a keyword


Things I would like to learn:

 - Offer you a recipe saved by another person
 - Add customized keywords to a recipe


