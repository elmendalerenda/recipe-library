const express  = require('express')
const { Telegraf } = require('telegraf')
const fetchTitle = require('./fetch-title')
const { saveRecipe, findRecipe } = require('./recipes')

const port = process.env.PORT || 3000;
const token = process.env.BOT_TOKEN
if (token === undefined) {
  throw new Error('BOT_TOKEN must be provided!')
}

const bot = new Telegraf(token)
// Set the bot response
//bot.on('text', (ctx) => ctx.replyWithHTML('👋❤️✨'))

bot.help((ctx) => ctx.reply('Mándame una url para que la guarde'))
bot.command('cambiaretiquetas', (ctx) => ctx.reply('Envíame el número de receta y las nuevas etiquetas. Por ejemplo /cambiaretiquetas 42 arroz leche'))

bot.on('text', async (ctx) => {
  var isUrl = /^(https):\/\/[^ "]+$/.test(ctx.message.text);
  if(isUrl){
    try{
      const title = await fetchTitle(ctx.message.text)
      ctx.reply(`me la guardo, el título es ${title}`)
      saveRecipe(ctx.from.id, title, ctx.message.text)
    }
    catch(err) {
      console.log(err)
      ctx.reply('algo pasa con este enlace que no puedo leer el título')
    }
  }
  else {
    if (ctx.message.text === 'perro') {
      ctx.replyWithAnimation('https://media.giphy.com/media/VbtPJDnJs4qtDi9VWX/giphy.gif')
    }
    const found = await findRecipe(ctx.from.id, ctx.message.text)
    if(!found)
      ctx.reply(`no he encontrado ninguna receta de ${ctx.message.text}`)
    else
      ctx.replyWithHTML(`<a href="${found.url}">${found.title}. Receta número ${found.id}</a>`)
  }
})

const secretPath = "/lentejasdebot/my_little_secret"

// Set telegram webhook
// npm install -g localtunnel && lt --port 3000
// bot.telegram.setWebhook(`https://----.localtunnel.me${secretPath}`)

const app = express()
app.get('/', (req, res) => res.send('Hello World!'))
// Set the bot API endpoint

app.use(bot.webhookCallback(secretPath))
app.listen(port, () => {
  console.log('lentejasdebot is alive!')
})

bot.launch()
