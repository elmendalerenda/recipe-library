//script for testing purposes in local environment
// $> BOT_TOKEN=...node console.js

const { Telegraf, Markup } = require('telegraf')
const fetchTitle = require('./fetch-title')
const { saveRecipe, findRecipe } = require('./recipes')

if (process.env.BOT_TOKEN === undefined) {
  throw new TypeError('BOT_TOKEN must be provided!. run $> BOT_TOKEN=... node console.js')
}

const keyboard = Markup.inlineKeyboard([
  Markup.button.url('❤️', 'http://telegraf.js.org'),
  Markup.button.callback('Delete', 'delete')
])

const bot = new Telegraf(process.env.BOT_TOKEN)
bot.start((ctx) => ctx.reply('Hello'))
// bot.help((ctx) => ctx.reply('Help message'))
//bot.on('message', (ctx) => ctx.telegram.sendCopy(ctx.message.chat.id, ctx.message, keyboard))
//bot.on('text', (ctx) => ctx.telegram.reply(ctx.message.chat.id, 'Hello world'))
bot.help((ctx) => ctx.reply('Tienes todos los comandos disponibles en el botón Menu'))
bot.command('cambiaretiquetas', (ctx) => ctx.reply('Hello cambiar etiquetas'))
bot.on('text', async (ctx) => {

  console.log(ctx.message)
  var isUrl = /^(https):\/\/[^ "]+$/.test(ctx.message.text);
  if(isUrl){
    const title = await fetchTitle(ctx.message.text)
    ctx.reply('me la guardo, el titulo es ' + title)
    saveRecipe(ctx.from.id, title, ctx.message.text)
  }
  else {
    const found = await findRecipe(ctx.from.id, ctx.message.text)
    ctx.replyWithHTML(`<a href="${found.url}">${found.title}, Id ${found.id}</a>`)

    // ctx.replyWithHTML('👋❤️✨')
  }

  })

bot.action('delete', (ctx) => ctx.deleteMessage())

console.log('send messages via telegram, the bot is waiting. Remember to set the maintance mode in Heroku')
bot.launch()

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))
