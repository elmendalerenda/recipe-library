const axios = require("axios");
const cheerio = require('cheerio');

const fetchTitle = async (url) => {
    const response = await axios.get(url)
    const $ = cheerio.load(response.data);
    const title = $('title')[0].children[0].data

    return title
// }).catch(err => {
//   console.log(err);
// })
}

module.exports = fetchTitle
