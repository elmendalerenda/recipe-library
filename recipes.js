const { Sequelize, Model, DataTypes } = require('sequelize');

const connection = async () => {
  let sequelize = null

  if (process.env.DATABASE_URL) {
    sequelize = new Sequelize(process.env.DATABASE_URL,
      {
        dialect: "postgres",
        dialectOptions: {
          ssl: {
            require: true,
            rejectUnauthorized: false
          }
        }
      }
    );
  }
  else {
    sequelize = new Sequelize({
      "dialect": "sqlite",
      "storage": "./database.sqlite3"
    })
  }

  try {
    await sequelize.authenticate()
    console.log('Connection has been established successfully.');
    return sequelize
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

const saveRecipe = async (user, title, url) => {
  const sequelize = await connection()
  const Recipe = await recipeModel(sequelize)

  Recipe.create({ url, title, user }).then((recipe) => {
  }).catch(err => {
    console.log(err);
 });
}


const recipeModel = async (sequelize) => {
  const Recipe = sequelize.define('Recipe', {
    url: {
      type: DataTypes.STRING,
      allowNull: false
    },
    title: {
      type: DataTypes.STRING,
      allowNull: false
    },
    user: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {});

  await Recipe.sync()

  return Recipe
}

const findRecipe = async (user, searchTerm) => {
  const sequelize = await connection()
  const Recipe = await recipeModel(sequelize)

  const results = await Recipe.findAll({
    where: {
      user: user.toString(),
      title: sequelize.where(sequelize.fn('LOWER', sequelize.col('title')), 'LIKE', '%' + searchTerm.toLowerCase() + '%')
    }
  })

  if(results.length === 0)
    return null
  else
    return results[0]
}
module.exports = { saveRecipe, findRecipe }
